# Elastic Stack 

This is a collection of guides on how to use the [Elastic Stack](https://www.elastic.co/elastic-stack)

## How it works
![schema](doc/schemas/elkSchema.png)

The basic architecture of the ELK stack contains:

* Logstash - Collects logs from the application
* Elasticsearch - Persists the logs
* Kibana - Visualizes the logs

## Compose

To run the Stack you have to run: `docker-compose up --build` inside the compose directory.

### How it works

Logstash exposes a [GELF](https://docs.graylog.org/en/3.2/pages/gelf.html) endpoint and sends the logs to the elasticsearch with the given host and port.

```conf
input {
  gelf {
    port => 12201
  }
}
output {
  stdout {}
  elasticsearch {
    hosts => ["http://elasticsearch:9200"]
  }
}
```

