package de.openknowledge.demo.app;

import org.jboss.logging.Logger;

import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

@Path("/hello")
@Singleton
public class HelloController {
  private static final Logger LOG = Logger.getLogger(HelloController.class);

  @GET
  public String sayHello() {
    LOG.info("Hello World");
    return "Hello World";
  }

  @GET
  @Path("{name}")
  public Response sayNamedHello(@PathParam("name") final String name) {
    if (name.equals("warn")) {
      LOG.warn("name failed");
      return Response.status(Response.Status.BAD_REQUEST).entity("name failed").build();
    }
    if (name.equals("error")) {
      LOG.error("name failed");
      return Response.status(Response.Status.BAD_REQUEST).entity("name failed").build();
    }
    LOG.info("Hello " + name);
    return Response.ok().entity("Hello " + name).build();
  }

  @GET
  @Path("/exceptions/{message}")
  public Response sayHelloException(@PathParam("message") final String message) {
    try {
      if (message.equals("null")) {
        throw new NullPointerException("nullpointerexception message");
      }
      if (message.equals("illegal")) {
        throw new IllegalArgumentException("illegalargumentexception message");
      }

      LOG.info("200 " + message);
      return Response.ok().entity(message).build();
    } catch (NullPointerException n) {
      LOG.info("400", n.getMessage(), n);
      return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
    } catch (IllegalArgumentException i) {
      LOG.info("404", i.getMessage(), i);
      return Response.status(Response.Status.NOT_FOUND).entity(message).build();
    }
  }
}
