package de.openknowledge.demo.app;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 *
 */
@ApplicationPath("/data")
public class DemoappRestApplication extends Application {
}
